# https://www.gitpod.io/docs/configure/workspaces/workspace-image#use-a-private-docker-image
# https://hub.docker.com/r/gitpod/workspace-full/tags
FROM gitpod/workspace-full:latest

# Install custom tools, runtime, etc.

# kubectl, envsubst
RUN sudo mkdir -p /etc/apt/keyrings/
RUN sudo curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
RUN sudo install-packages gettext-base kubectl
RUN kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null

# doctl
RUN wget -nv https://github.com/digitalocean/doctl/releases/download/v1.86.0/doctl-1.86.0-linux-amd64.tar.gz && \
tar xf doctl-1.86.0-linux-amd64.tar.gz && \
sudo install -o root -g root -m 0755 doctl /usr/local/bin/doctl && \
rm -rf doctl*

# eksctl
RUN wget -nv -O eksctl.tar.gz  "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" && \
tar xf eksctl.tar.gz && \
sudo install -o root -g root -m 0755 eksctl /usr/local/bin/eksctl && \
rm -rf eksctl*

# aws cli
RUN wget -nv -O "awscliv2.zip" "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" && \
unzip -qq awscliv2.zip && \
sudo ./aws/install && \
rm -rf aws*



